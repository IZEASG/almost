--
-- Built with,
--
--        ,gggg,
--       d8" "8I                         ,dPYb,
--       88  ,dP                         IP'`Yb
--    8888888P"                          I8  8I
--       88                              I8  8'
--       88        gg      gg    ,g,     I8 dPgg,
--  ,aa,_88        I8      8I   ,8'8,    I8dP" "8I
-- dP" "88P        I8,    ,8I  ,8'  Yb   I8P    I8
-- Yb,_,d88b,,_   ,d8b,  ,d8b,,8'_   8) ,d8     I8,
--  "Y8P"  "Y888888P'"Y88P"`Y8P' "YY8P8P88P     `Y8
--

-- This is a starter colorscheme for use with Lush,
-- for usage guides, see :h lush or :LushRunTutorial

--
-- Note: Because this is lua file, vim will append your file to the runtime,
--       which means you can require(...) it in other lua code (this is useful),
--       but you should also take care not to conflict with other libraries.
--
--       (This is a lua quirk, as it has somewhat poor support for namespacing.)
--
--       Basically, name your file,
--
--       "super_theme/lua/lush_theme/super_theme_lark.lua",
--
--       not,
--
--       "super_theme/lua/dark.lua".
--
--       With that caveat out of the way...
--

-- Enable lush.ify on this file, run:
--
--  `:Lushify`
--
--  or
--
--  `:lua require('lush').ify()`

local lush = require('lush')
local hsl = lush.hsl
local nvim_bg = vim.o.background

local light = hsl(120, 10, 96)
local dark = hsl(110, 10, 12)

local mn_100__d = dark.li(40)
local mn_200__d = dark.li(50)
local mn_300__d = dark.li(60)
local mn_400__d = dark.li(70)

local fg = light
local bg = dark

local clr_100 = fg.ro(225).sa(80).da(20)
local clr_200 = fg.ro(-5).sa(80).da(20)
local clr_300 = fg.ro(-75).sa(80).da(20)
local clr_400 = fg.ro(90).sa(80).da(20)
local clr_500 = fg.ro(163).sa(80).da(20)

local clr_100__bg = clr_100.mix(bg, 85)
local clr_200__bg = clr_200.mix(bg, 85)
local clr_300__bg = clr_300.mix(bg, 85)
local clr_400__bg = clr_400.mix(bg, 85)
local clr_500__bg = clr_500.mix(bg, 85)

local mn_base = fg.mix(bg, 50).sa(15)
local mn_100 = mn_base.li(50)
local mn_200 = mn_base.li(37)
local mn_300 = mn_base.li(25)

local cursor_line = fg.mix(bg, 95)

local comment = fg.da(50)
local fold = fg.da(30)
local menu = fg.da(100)
local select = fg.da(60)
local status = fg.da(70)

local cursor = hsl(0, 0, 80)

if nvim_bg == nil then
    nvim_bg = 'dark'
    vim.opt.background = nvim_bg
end

-- if nvim_bg == 'light' then
--     fg = dark
--     bg = light
-- end

-- local theme = lush(function()
local theme = lush(function(injected_functions)
  local sym = injected_functions.sym
  return {
    -- The following are all the Neovim default highlight groups from the docs
    -- as of 0.5.0-nightly-446, to aid your theme creation. Your themes should
    -- probably style all of these at a bare minimum.
    --
    -- Referenced/linked groups must come before being referenced/lined,
    -- so the order shown ((mostly) alphabetical) is likely
    -- not the order you will end up with.
    --
    -- You can uncomment these and leave them empty to disable any
    -- styling for that group (meaning they mostly get styled as Normal)
    -- or leave them commented to apply vims default colouring or linking.

    -- comment
    Comment      { fg = comment, gui = "italic" }, -- any comment

    --cursor
    Cursor       { fg = bg, bg = cursor }, -- character under the cursor
    lCursor      { Cursor }, -- the character under the cursor when |language-mapping| is used (see 'guicursor')
    CursorIM     { fg = bg, bg = clr_500 }, -- like Cursor, but used when in IME mode |CursorIM|
    CursorLine   { bg = cursor_line }, -- Screen-line at the cursor, when 'cursorline' is set.  Low-priority if foreground (ctermfg OR guifg) is not set.
    CursorColumn { CursorLine }, -- Screen-column at the cursor, when 'cursorcolumn' is set.
    TermCursor   { Cursor }, -- cursor in a focused terminal
    TermCursorNC { Cursor, gui = "reverse" }, -- cursor in an unfocused terminal

    -- diff
    DiffAdd      { fg = clr_200 }, -- diff mode: Added line |diff.txt|
    DiffChange   { fg = clr_300 }, -- diff mode: Changed line |diff.txt|
    DiffDelete   { fg = clr_100 }, -- diff mode: Deleted line |diff.txt|
    DiffText     { fg = clr_400 }, -- diff mode: Changed text within a changed line |diff.txt|

    -- folds
    Folded       { fg = fold }, -- line used for closed folds
    FoldColumn   { Folded }, -- 'foldcolumn'

    -- search
    Search       { fg = clr_300, bg = clr_300__bg, gui = "bold" }, -- Last search pattern highlighting (see 'hlsearch').  Also used for similar items that need to stand out.
    IncSearch    { fg = Search.fg, gui = "underline" }, -- 'incsearch' highlighting; also used for the text replaced with ":s///c"
    -- Substitute   { }, -- |:substitute| replacement text highlighting

    -- line number
    LineNr       { fg = status }, -- Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
    CursorLineNr { fg = clr_200, bg = cursor_line, gui = "bold" }, -- Like LineNr when 'cursorline' or 'relativenumber' is set for the cursor line.

    MatchParen   { fg = clr_200 }, -- The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
    ModeMsg      { fg = clr_200, gui = "bold" }, -- 'showmode' message (e.g., "-- INSERT -- ")
    -- MsgArea      { }, -- Area for messages and cmdline
    -- MsgSeparator { }, -- Separator for scrolled messages, `msgsep` flag of 'display'
    MoreMsg      { MatchParen }, -- |more-prompt|

    -- text
    Normal       { fg = fg, bg = bg }, -- normal text
    NormalNC     { Normal }, -- normal text in non-current windows
    NormalFloat  { fg = fg, bg = cursor_line }, -- Normal text in floating windows.

    -- pmenu
    Pmenu        { fg = fg, bg = menu }, -- Popup menu: normal item.
    PmenuSel     { Pmenu, gui = "reverse" }, -- Popup menu: selected item.
    PmenuSbar    { bg = Pmenu.bg }, -- Popup menu: scrollbar.
    PmenuThumb   { bg = Pmenu.fg }, -- Popup menu: Thumb of the scrollbar.

    -- spellcheck
    SpellBad     { fg = clr_100, gui = "underline,bold" }, -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise. 
    SpellCap     { fg = clr_200, gui = "underline,bold" }, -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
    SpellLocal   { fg = clr_300, gui = "underline,bold" }, -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
    SpellRare    { fg = clr_400, gui = "underline,bold" }, -- Word that is recognized by the spellchecker as one that is hardly ever used.  |spell| Combined with the highlighting used otherwise.

    -- statusline
    StatusLine   { fg = bg, bg = fg }, -- status line of current window
    StatusLineNC { fg = fg, bg = status }, -- status lines of not-current windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
    WildMenu     { StatusLine, gui = "reverse" }, -- current match in 'wildmenu' completion

    -- tabs
    TabLine      { fg = fg, bg = bg }, -- tab pages line, not active tab page label
    TabLineFill  { TabLine }, -- tab pages line, where there are no labels
    TabLineSel   { StatusLine }, -- tab pages line, active tab page label

    -- visual selection
    Visual       { fg = fg, bg = select }, -- Visual mode selection
    VisualNOS    { Visual }, -- Visual mode selection when vim is "Not Owning the Selection".

    -- error/warning
    Error        { fg = clr_100 }, -- (preferred) any erroneous construct
    ErrorMsg     { fg = Error.fg, bg = clr_100__bg, gui = "bold" }, -- error messages on the command line
    WarningMsg   { fg = clr_300, bg = clr_300__bg, gui = "bold" }, -- warning messages

    -- misc
    ColorColumn  { fg = clr_100, bg = cursor_line }, -- used for the columns set with 'colorcolumn'
    Conceal      { fg = bg }, -- placeholder characters substituted for concealed text (see 'conceallevel')
    Directory    { fg = clr_400, gui = "bold" }, -- directory names (and other special names in listings)
    EndOfBuffer  { fg = status }, -- filler lines (~) after the end of the buffer.  By default, this is highlighted like |hl-NonText|.
    NonText      { EndOfBuffer }, -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
    Question     { fg = clr_300 }, -- |hit-enter| prompt and yes/no questions
    QuickFixLine { fg = bg, bg = fg, gui = "bold" }, -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
    SignColumn   { bg = bg }, -- column where |signs| are displayed
    SpecialKey   { fg = clr_300 }, -- Unprintable characters: text displayed differently from what it really is.  But not 'listchars' whitespace. |hl-Whitespace|
    Title        { fg = fg, gui = "bold" }, -- titles for output from ":set all", ":autocmd" etc.
    VertSplit    { fg = status, bg = status }, -- the column separating vertically split windows
    Whitespace   { EndOfBuffer }, -- "nbsp", "space", "tab" and "trail" in 'listchars'

    -- These groups are not listed as default vim groups,
    -- but they are defacto standard group names for syntax highlighting.
    -- commented out groups should chain up to their "preferred" group by
    -- default,
    -- Uncomment and edit if you want more specific syntax highlighting.

    Constant       { fg = fg }, -- red (preferred) any constant
    -- String         { }, --  a string constant: "this is a string"
    -- Character      { }, --  a character constant: 'c', '\n'
    -- Number         { }, --  a number constant: 234, 0xff
    -- Boolean        { }, --  a boolean constant: TRUE, false
    -- Float          { }, --  a floating point constant: 2.3e10
    Identifier     { fg = mn_100 }, -- cyan (preferred) any variable name
    Function       { Identifier, gui="italic" }, -- require function name (also: methods for classes)
    Statement      { fg = mn_100 }, -- yellow (preferred) any statement
    -- Conditional    { }, --  if, then, else, endif, switch, etc.
    -- Repeat         { }, --   for, do, while, etc.
    -- Label          { }, --    case, default, etc.
    -- Operator       { }, -- "sizeof", "+", "*", etc.
    -- Keyword        { }, --  any other keyword
    -- Exception      { }, --  try, catch, throw
    PreProc        { fg = mn_200, gui = "italic" }, -- (preferred) generic Preprocessor
    -- Include        { }, --  preprocessor #include
    -- Define         { }, --   preprocessor #define
    -- Macro          { }, --    same as Define
    -- PreCondit      { }, --  preprocessor #if, #else, #endif, etc.
    Type           { fg = mn_300 }, -- (preferred) int, long, char, etc.
    -- StorageClass   { }, -- static, register, volatile, etc.
    -- Structure      { }, --  struct, union, enum, etc.
    -- Typedef        { }, --  A typedef
    Special        { fg = mn_300, gui= "italic" }, -- (preferred) any special symbol
    -- SpecialChar    { }, --  special character in a constant
    -- Tag            { }, --    you can use CTRL-] on this
    Delimiter      { fg = mn_300__d }, --  character that needs attention
    -- SpecialComment { }, -- special things inside a comment
    -- Debug          { }, --    debugging statements

    Underlined   { gui = "underline" }, -- (preferred) text that stands out, HTML links
    Bold         { gui = "bold" },
    Italic       { gui = "italic" },

    -- ("Ignore", below, may be invisible...)
    -- Ignore         { }, -- (preferred) left blank, hidden  |hl-Ignore|

    Todo         { fg = clr_300, gui = "bold" }, -- (preferred) anything that needs extra attention; mostly the keywords TODO FIXME and XXX

    -- These groups are for the native LSP client. Some other LSP clients may
    -- use these groups, or use their own. Consult your LSP client's
    -- documentation.

    -- LspReferenceText                     { }, -- used for highlighting "text" references
    -- LspReferenceRead                     { }, -- used for highlighting "read" references
    -- LspReferenceWrite                    { }, -- used for highlighting "write" references
    -- LspCodeLens                          { }, -- Used to color the virtual text of the codelens. See |nvim_buf_set_extmark()|.
    -- LspCodeLensSeparator                 { }, -- Used to color the seperator between two or more code lens.
    -- LspSignatureActiveParameter          { }, -- Used to highlight the active parameter in the signature help. See |vim.lsp.handlers.signature_help()|.

    DiagnosticDefaultError           { fg = clr_100 }, -- Used as the base highlight group. Other LspDiagnostic highlights link to this by default (except Underline)
    DiagnosticDefaultWarn            { fg = clr_300 }, -- Used as the base highlight group. Other LspDiagnostic highlights link to this by default (except Underline)
    DiagnosticDefaultInfo            { fg = clr_200 }, -- Used as the base highlight group. Other LspDiagnostic highlights link to this by default (except Underline)
    DiagnosticDefaultHint            { fg = clr_400 }, -- Used as the base highlight group. Other LspDiagnostic highlights link to this by default (except Underline)

    DiagnosticVirtualTextError       { DiagnosticDefaultError }, -- Used for "Error" diagnostic virtual text
    DiagnosticVirtualTextWarn        { DiagnosticDefaultWarn }, -- Used for "Warning" diagnostic virtual text
    DiagnosticVirtualTextInfo        { DiagnosticDefaultInfo }, -- Used for "Information" diagnostic virtual text
    DiagnosticVirtualTextHint        { DiagnosticDefaultHint }, -- Used for "Hint" diagnostic virtual text

    DiagnosticUnderlineError         { DiagnosticDefaultError, gui = "underline" }, -- Used to underline "Error" diagnostics
    DiagnosticUnderlineWarn          { DiagnosticDefaultWarn, gui = "underline" }, -- Used to underline "Warning" diagnostics
    DiagnosticUnderlineInfo          { DiagnosticDefaultInfo, gui = "underline" }, -- Used to underline "Information" diagnostics
    DiagnosticUnderlineHint          { DiagnosticDefaultHint, gui = "underline" }, -- Used to underline "Hint" diagnostics

    DiagnosticFloatingError          { DiagnosticDefaultError }, -- Used to color "Error" diagnostic messages in diagnostics float
    DiagnosticFloatingWarn           { DiagnosticDefaultWarn }, -- Used to color "Warning" diagnostic messages in diagnostics float
    DiagnosticFloatingInfo           { DiagnosticDefaultInfo }, -- Used to color "Information" diagnostic messages in diagnostics float
    DiagnosticFloatingHint           { DiagnosticDefaultHint }, -- Used to color "Hint" diagnostic messages in diagnostics float

    DiagnosticSignError              { DiagnosticDefaultError }, -- Used for "Error" signs in sign column
    DiagnosticSignWarn               { DiagnosticDefaultWarn }, -- Used for "Warning" signs in sign column
    DiagnosticSignInfo               { DiagnosticDefaultInfo }, -- Used for "Information" signs in sign column
    DiagnosticSignHint               { DiagnosticDefaultHint }, -- Used for "Hint" signs in sign column

    -- These groups are for the neovim tree-sitter highlights.
    -- As of writing, tree-sitter support is a WIP, group names may change.
    -- By default, most of these groups link to an appropriate Vim group,
    -- sym("@Error -> Error for example, so you do not have to define these unless
    -- you explicitly want to support Treesitter's improved syntax awareness.

    --- misc
    -- sym("@comment")                  { } , -- line and block comments.
    -- sym("@error")                    { } , -- syntax/parser errors.
    -- sym("@none")                     { } , -- completely disable the highlight.
    -- sym("@preproc")                  { } , -- various preprocessor directives & shebangs.
    -- sym("@define")                   { } , -- preprocessor definition directives.
    -- sym("@operator")                 { } , -- symbolic operators (e.g. `+` / `*`).

    --- punctuation
    sym("@punctuation.delimiter")    { Delimiter } , -- delimiters (e.g. `;` / `.` / `,`).
    sym("@punctuation.bracket")      { Delimiter } , -- brackets (e.g. `()` / `{}` / `[]`).
    -- sym("@punctuation.special")      { } , -- special symbols (e.g. `{}` in string interpolation).

    ---literals
    -- sym("@string")                   { } , -- string literals.
    -- sym("@string.regex")             { } , -- regular expression.
    -- sym("@string.escape")            { } , -- escape sequences.
    -- sym("@string.special")           { } , -- other special strings (e.g. dates)

    -- sym("@character")                { } , -- character literals.
    -- sym("@character.special")        { } , -- special characters (e.g. wildcards).

    -- sym("@boolean")                  { } , -- boolean literals.
    -- sym("@number")                   { } , -- numeric literals.
    -- sym("@float")                    { } , -- floating-point number literals.

    --- functions
    -- sym("@function")                 { } , --  function definitions.
    -- sym("@function.builtin")         { } , -- built-in functions.
    -- sym("@function.call")            { } , -- function calls
    -- sym("@function.macro")           { } , -- preprocessor macros.

    -- sym("@method")                   { } , -- method definitions.
    -- sym("@method.call")              { } , -- method calls.

    -- sym("@Constructor")              { } , -- constructor calls and definitions.
    -- sym("@Parameter")                { } , -- parameters of a function.

    --- keywords
    -- sym("@keyword")                  { } , -- various keywords.
    -- sym("@keyword.function")         { } , -- keywords that define a function (e.g. `func` in Go, `def` in Python).
    -- sym("@keyword.operator")         { } , -- operators that are English words (e.g. `and` / `or`)
    -- sym("@keyword.return")           { } , -- keywords like `return` and `yield`.

    -- sym("@conditional")              { } , -- keywords related to conditionals (e.g. `if` / `else`).
    -- sym("@repeat")                   { } , -- keywords related to loops (e.g. `for` / `while`).
    -- sym("@debug")                    { } , -- keywords related to debugging.
    -- sym("@label")                    { } , -- GOTO and other labels (e.g. `label:` in C).
    -- sym("@include")                  { } , -- keywords for including modules (e.g. `import` / `from` in Python)
    -- sym("@exception")                { } , -- keywords related to exceptions (e.g. `throw` / `catch`).

    ---types
    -- sym("@type")                     { } , -- type or class definitions and annotations.
    -- sym("@type.builtin")             { } , -- built-in types.
    -- sym("@type.definition")          { } , -- type definitions (e.g. `typedef` in C).
    -- sym("@type.qualifier")           { } , -- type qualifiers (e.g. `const`).

    -- sym("@storageclass")             { } , -- visibility/life-time/etc. modifiers (e.g. `static`).
    -- sym("@attribute")                { } , -- attribute annotations (e.g. Python decorators).
    -- sym("@field")                    { } , -- object and struct fields.
    -- sym("@property")                 { } , -- similar to `@field`.

    --- identifiers
    -- sym("@variable")                 { } , -- various variable names.
    -- sym("@variable.builtin")         { } , -- built-in variable names (e.g. `this`).

    -- sym("@constant")                 { } , -- constant identifiers.
    -- sym("@constant.builtin")         { } , -- built-in constant values.
    -- sym("@constant.macro")           { } , -- constants defined by the preprocessor.

    -- sym("@namespace")                { } , -- modules or namespaces.
    -- sym("@symbol")                   { } , -- symbols or atoms.

    --- text
    -- sym("@text")                     { } , -- non-structured text.
    sym("@text.strong")              { gui = "bold" } , -- bold text.
    sym("@text.emphasis")            { gui = "italic" } , -- text with emphasis.
    sym("@text.underline")           { gui = "underline" } , -- underlined text.
    -- sym("@text.strike")              { } , -- strikethrough text.
    -- sym("@text.title")               { } , -- text that is part of a title.
    -- sym("@text.literal")             { } , -- literal or verbatim text.
    -- sym("@text.uri")                 { } , -- URIs (e.g. hyperlinks).
    -- sym("@text.math")                { } , -- math environments (e.g. `$ ... $` in LaTeX).
    -- sym("@text.environment")         { } , -- text environments of markup languages.
    -- sym("@text.environment.name")    { } , -- text indicating the type of an environment.
    -- sym("@text.text.reference")      { } , -- text references, footnotes, citations, etc.

    sym("@text.todo")                { fg = clr_300, gui = "bold" } , -- todo notes.
    sym("@text.note")                { fg = clr_200, gui = "bold" } , -- info notes.
    sym("@text.warning")             { fg = clr_300, gui = "bold" } , -- warning notes.
    sym("@text.danger")              { fg = clr_100, gui = "bold" } , -- danger/error notes.

    --- tags
    sym("@tag")                      { Special } , -- XML tag names.
    sym("@tag.attribute")            { fg = mn_200__d } , -- XML tag attributes.
    sym("@tag.delimiter")            { Delimiter } , -- XML tag delimiters.

    --netrw
   netrwPlain           { fg = fg },
   netrwClassify        { fg = bg },
   netrwExe             { fg = clr_200 },
   netrwSpecial         { fg = clr_500, gui = "bold" },

    -- latex
    texDocType          { Type },
    -- texDocTypeArgs      { },
    -- texBeginEndName     { },
    -- texInputFile        { },
    -- texInputFileOpt     { },
    -- texStatement        { },
    -- texSection          { },
    -- texBeginEnd         { },
    -- texLength           { },
    -- texRefZone          { },
    texMathZones         { fg = clr_100 },
    texMathZoneA         { texMathZones },
    texMathZoneAS        { texMathZones },
    texMathZoneB         { texMathZones },
    texMathZoneBS        { texMathZones },
    texMathZoneC         { texMathZones },
    texMathZoneCS        { texMathZones },
    texMathZoneD         { texMathZones },
    texMathZoneDS        { texMathZones },
    texMathZoneE         { texMathZones },
    texMathZoneES        { texMathZones },
    texMathZoneF         { texMathZones },
    texMathZoneFS        { texMathZones },
    texMathZoneG         { texMathZones },
    texMathZoneGS        { texMathZones },
    texMathZoneH         { texMathZones },
    texMathZoneHS        { texMathZones },
    texMathZoneI         { texMathZones },
    texMathZoneIS        { texMathZones },
    texMathZoneJ         { texMathZones },
    texMathZoneJS        { texMathZones },
    texMathZoneK         { texMathZones },
    texMathZoneKS        { texMathZones },
    texMathZoneL         { texMathZones },
    texMathZoneLS        { texMathZones },
    texMathZoneV         { texMathZones },
    texMathZoneW         { texMathZones },
    texMathZoneX         { texMathZones },
    texMathZoneY         { texMathZones },
    texMathZoneZ         { texMathZones },

  }
end)

-- return our parsed theme for extension or use else where.
return theme

-- vi:nowrap
